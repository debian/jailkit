Source: jailkit
Section: utils
Priority: optional
Maintainer: Joao Eriberto Mota Filho <eriberto@debian.org>
Build-Depends: debhelper-compat (= 13), dh-python, python3:any
Standards-Version: 4.6.1
Rules-Requires-Root: no
Homepage: https://olivier.sessink.nl/jailkit/
Vcs-Browser: https://salsa.debian.org/debian/jailkit
Vcs-Git: https://salsa.debian.org/debian/jailkit.git

Package: jailkit
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, ${python3:Depends}
Description: tools to generate chroot jails easily
 Jailkit is a set of utilities to limit user accounts to specific files using
 chroot() and or specific commands. Setting up a chroot shell, a shell limited
 to some specific command or a daemon inside a chroot jail is a lot easier and
 can be automated using these utilities.
 .
 Jailkit is a specialized tool that is developed with a focus on security. It
 will abort in a secure way if the configuration, the system setup or the
 environment is not 100% secure, and it will send useful log messages that
 explain what is wrong to syslog.
 .
 Jailkit is known to be used in network security appliances from several
 leading IT security firms, Internet servers from several large enterprise
 organizations, Internet servers from Internet service providers, as well as
 many smaller companies and private users that need to secure login in services
 or in daemon processes.
 .
 Currently, Jailkit provide jails for cvs, git, scp sftp, ssh, rsync, procmail,
 openvpn, vnc, etc.
 .
 Jailkit make available the following commands: jk_check, jk_chrootlaunch,
 jk_chrootsh, jk_cp, jk_init, jk_jailuser, jk_list, jk_lsh, jk_socketd,
 jk_uchroot, jk_update.
